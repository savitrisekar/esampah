package com.example.esampah;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.esampah.custom.CustomBottomNavigationView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private FloatingActionButton fab;

    final Fragment homeFragment = new HomeFragment();
    final FragmentManager fragmentManager = getSupportFragmentManager();
    Fragment active = homeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        CustomBottomNavigationView customNav = findViewById(R.id.custom_navbar);
        customNav.inflateMenu(R.menu.bottom_menu);
        customNav.setSelectedItemId(R.id.menu_home);
        customNav.setOnNavigationItemSelectedListener(MainActivity.this);

        fab = findViewById(R.id.fab_scan);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Click me!", Toast.LENGTH_SHORT).show();
            }
        });

        fragmentManager.beginTransaction().add(R.id.main_container, homeFragment, "1").commit();
        getSupportActionBar().setTitle(getString(R.string.home));
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_home:
                fragmentManager.beginTransaction().hide(active).show(homeFragment).commit();
                active = homeFragment;
                getSupportActionBar().setTitle(getString(R.string.home));
                return true;
            case R.id.menu_history:
                break;
            case R.id.menu_favorite:
                break;
            case R.id.menu_saldo:
                break;
            case R.id.menu_profile:
                break;
        }
        return false;
    }
}
